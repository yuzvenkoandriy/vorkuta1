<?php
header('Access-Control-Allow-Origin: *');
?>
<?php
$url = isset($_GET['url']) ? $_GET['url'] : false; // здесь нам надо получить сам адрес вставки
if (!$url) die(); // если его нет - ничего не делаем
$url = urldecode($url); // расдекодим все его вопросики и апресанды после передачи
$content = file_get_contents($url); // вся магия - получаем содержимое айфрейма
$content = str_replace('</head>','<link rel="stylesheet" href="/css/fonts.css"><link rel="stylesheet" href="/css/main.css" /><link rel="stylesheet" href="/css/main-new.css" /></head>', $content); // производим все замены, в данном случаем перед закрывающемся <head> добавим новый файл стилей
$content = str_replace('xml2017.php','http://canadiansoccerleague.ca/standings/xml2017.php', $content);