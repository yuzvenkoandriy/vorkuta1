$(document).ready(function () {

    $('button.social-sharing').on('click', function () {
        type = $(this).attr('data-type');
        if (type.length) {
            switch (type) {
                case'twitter':
                    window.open('https://twitter.com/intent/tweet?text=' + sharing_name + ' ' + encodeURIComponent(sharing_url), 'sharertwt', 'toolbar=0,status=0,width=640,height=445');
                    break;
                case'facebook':
                    window.open('http://www.facebook.com/sharer.php?u=' + sharing_url, 'sharer', 'toolbar=0,status=0,width=660,height=445');
                    break;
                case'google-plus':
                    window.open('https://plus.google.com/share?url=' + sharing_url, 'sharer', 'toolbar=0,status=0,width=660,height=445');
                    break;
                case'pinterest':
                    var img_url = sharing_img;
                    if (typeof $("#bigpic").attr('src') != 'undefined' && $("#bigpic").attr('src') != '') {
                        img_url = $("#bigpic").attr('src');
                    }
                    window.open('http://www.pinterest.com/pin/create/button/?media=' + img_url + '&url=' + sharing_url, 'sharerpinterest', 'toolbar=0,status=0,width=660,height=445');
                    break;
            }
        }
    });


    $('.js-height').matchHeight();


    var tabs = $('#tabs');
    $('.tabs-content > div', tabs).each(function(i){
        if ( i != 0 ) $(this).hide(0);
    });
    tabs.on('click', '.tabs a', function(e){
        e.preventDefault();
        var tabId = $(this).attr('href');
        $('.tabs a',tabs).removeClass();
        $(this).addClass('active');
        $('.tabs-content > div', tabs).hide(0);
        $(tabId).show();
    });
    $('iframe').each(function(){
        function injectCSS(){
            $iframe.contents().find('head').append(
                $('<link/>', { rel: 'stylesheet', href: 'iframe.css', type: 'text/css' })
            );
        }

        var $iframe = $(this);
        $iframe.on('load', injectCSS);
        injectCSS();
    });

    $("#contactform").each(function() {
        var it = $(this);
        it.validate({
            rules: {
                firstname: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {},
            errorPlacement: function(error, element) {},
            submitHandler: function(form) {
                var thisForm = $(form);
                console.log(thisForm.serialize());
                $.ajax({
                    type: "POST",
                    url: thisForm.attr("action"),
                    data: thisForm.serialize()
                }).done(function() {
                    it.find("input").val("");
                    $.magnificPopup.open({
                        items: {
                            src: '#thanks'
                        }
                    });
                    console.log("good");
                    // setTimeout(function() {
                    //     $.fancybox.close();
                    // }, 3000);
                    $(".validate").trigger("reset");
                });
                return false;
            },
            success: function() {},
            highlight: function(element, errorClass) {
                $(element).addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('error');
            }
        })
    });

});