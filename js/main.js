$(document).ready(function () {

    $('.menu_toggle').click(function () {
        $('.navigation nav').slideToggle('fast');
        $(this).toggleClass('active');
    });
    $('.tabs_links a').click(function () {
        $('.tabs_links a').removeClass('active');
        $(this).addClass('active');
    });

    $('a[href^="#"]').bind("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 20
        }, 1000);
        e.preventDefault();
    });

    $('.main_slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 50000,
        dots: false,
        arrows: true,
        prevArrow: '<span class="m_prev"><img src="img/slideArrow.svg" alt=""></span>',
        nextArrow: '<span class="m_next"><img src="img/slideArrow.svg" alt=""></span>',
        responsive: [
            {
                breakpoint: 450,
                settings: {
                    arrows: false,
                }
            }
        ]
    });
    $('.main_team_carousel').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 50000,
        dots: false,
        arrows: true,
        prevArrow: '<span class="m_prev"><img src="img/slideArrow.svg" alt=""></span>',
        nextArrow: '<span class="m_next"><img src="img/slideArrow.svg" alt=""></span>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1 ,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    $('.team_carousel').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 50000,
        dots: false,
        arrows: true,
        prevArrow: '<span class="m_prev"><img src="img/slideArrow.svg" alt=""></span>',
        nextArrow: '<span class="m_next"><img src="img/slideArrow.svg" alt=""></span>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1 ,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    $('.news_items').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 50000,
        dots: false,
        arrows: true,
        prevArrow: '<span class="m_prev"><img src="img/slideArrow.svg" alt=""></span>',
        nextArrow: '<span class="m_next"><img src="img/slideArrow.svg" alt=""></span>',
        responsive: [
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1 ,
                    slidesToScroll: 1,
                }
            }
        ]
    });
});